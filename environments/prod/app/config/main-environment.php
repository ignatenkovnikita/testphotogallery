<?php
// production environment configuration
return array(
	'components' => array(
		'db' => array(
			'connectionString' => 'mysql:host=localhost;dbname=petun_realtyvyksaorg',
			'username' => 'petun',
			'password' => '',
			'charset' => 'utf8',
		),
	),
);
