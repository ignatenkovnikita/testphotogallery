<?php
// development environment configuration
return array(
	'modules' => array(
		'gii' => array(
			'class' => 'system.gii.GiiModule',
			'password' => 'gii',
			'ipFilters' => array('*'),
			'generatorPaths' => array('vendor.crisu83.yiistrap.gii', 'ext.giix-core'),
		),
	),
	'components' => array(
		'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=testPhotoGallery',
            'username' => 'root',
            'password' => 'root',
            'charset' => 'utf8',
			'enableProfiling' => true,
			'enableParamLogging' => true,
		),
	),
);
