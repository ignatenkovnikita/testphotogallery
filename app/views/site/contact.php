<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form TbActiveForm */

$this->pageTitle = app()->name . ' - Contact';
$this->breadcrumbs = array(
    'Contact',
);
?>

    <h1>Contact</h1>

    <?php if (user()->hasFlash('contact')): ?>
        <?php $this->widget('bootstrap.widgets.TbAlert', array('alerts' => array('contact' => array()))); ?>
    <?php endif;?>



<div class="span4">
    <aside>
        <div class="widget">
            <h4><?=t('app', 'Get in touch with us')?></h4>
            <ul>
                <!--<li><label><strong><?/*=t('app', 'Phone : ')*/?></strong></label>
                    <p>
                        +900 888 707 123
                    </p>
                </li>-->
                <li><label><strong><?=t('app', 'Email : ')?></strong></label>
                    <p>
                        <a href="mailto:info@vyksaotzyvy.ru">info@vyksaotzyvy.ru</a>
                    </p>
                </li>
                <li><label><strong><?=t('app', 'Adress : ')?></strong></label>
                    <p>
                        Россия, Нижегородская область, Выкса
                    </p>
                </li>
            </ul>
        </div>
        <div class="widget">
            <h4><?=t('app', 'Social network')?></h4>
            <ul class="social-links">
                <li><a href="https://twitter.com/vyksaotzyvy" title="Twitter"><i class="icon-rounded icon-32 icon-twitter"></i></a></li>
                <li><a href="contact.html#" title="Facebook"><i class="icon-rounded icon-32 icon-facebook"></i></a></li>
                <li><a href="contact.html#" title="Google plus"><i class="icon-rounded icon-32 icon-google-plus"></i></a></li>
                <li><a href="contact.html#" title="Linkedin"><i class="icon-rounded icon-32 icon-linkedin"></i></a></li>
                <li><a href="contact.html#" title="Pinterest"><i class="icon-rounded icon-32 icon-pinterest"></i></a></li>
            </ul>
        </div>
    </aside>
</div>
<div class="span8">
    <div class="map-container">
        <script type="text/javascript" charset="utf-8" src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=yJESvi6o_x7CuxzSYx9A92GEIq8PLfq6&width=600&height=450"></script>    </div>
    <div class="spacer30">
    </div>
    <?php $form = $this->beginWidget(
        'bootstrap.widgets.TbActiveForm',
        array(
            'id' => 'contact-form',
            'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
            'htmlOptions'=>array('class'=>'contact-form')
        )
    ); ?>


    <div class="row">
        <?php echo $form->errorSummary($model); ?>
        <div class="span3">
            <?php echo $form->labelEx($model, 'name'); ?>
            <?php echo $form->textField($model, 'name', array('class'=>'input-block-level', 'placeholder'=>'Your name')); ?>
        </div>
        <div class="span3">
            <?php echo $form->labelEx($model, 'email'); ?>
            <?php echo $form->textField($model, 'email', array('class'=>'input-block-level', 'placeholder'=>'Your email')); ?>
        </div>
        <div class="span6">
            <?php echo $form->labelEx($model, 'subject'); ?>
            <?php echo $form->textField($model, 'subject', array('class'=>'input-block-level', 'placeholder'=>'Your subject')); ?>
        </div>
        <div class="span8">
            <?php echo $form->labelEx($model, 'body'); ?>
            <?php echo $form->textArea($model, 'body', array('rows' => 9, 'class' => 'input-block-level', 'placeholder'=>'Your message')); ?>
            <?php echo TbHtml::submitButton('Submit', array('class' => 'btn btn-small btn-success')); ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>


</div>
</div>