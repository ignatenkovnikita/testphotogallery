<?php
/* @var SiteController $this */
/* @var array $error */
/* @var integer $code */
/* @var string $message */

$this->pageTitle = app()->name . ' - Error';
?>



<div class="span12">
    <div class="centered">
        <h2 class="error">404</h2>
        <h3>Sorry, that page doesn't exist!</h3>
        <p>
            The page you are looking for might have been removed, had its name changed, or is temporarily unavailable.
        </p>
    </div>
</div>