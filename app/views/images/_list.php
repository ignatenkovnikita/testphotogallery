<?php
/* @var $this ImagesController */
/* @var $model Images */



?>

<li class="span3">
    <div class="thumbnail">
        <?php echo CHtml::image(baseUrl().'uploads/'.$data->photo, 'sdf' ,array('style'=>'width:300px;height:200px') );?>

        <div class="caption">

            <p><?=$data->description;?></p>
            <p><?=$data->ShowRating ?> </p>
            <?php echo CHtml::link(t('app','Comment'),array('images/view', 'id'=>$data->id))?>
        </div>
    </div>
</li>