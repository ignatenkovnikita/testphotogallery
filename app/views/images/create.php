<?php
/* @var $this ImagesController */
/* @var $model Images */
?>

<?php
$this->breadcrumbs=array(
	Yii::t('app', 'Images')=>array('/'),
	Yii::t('app', 'Create'),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'List Images'), 'url'=>array('index')),
);
?>

<h1><?php echo Yii::t('app','Create Images'); ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>