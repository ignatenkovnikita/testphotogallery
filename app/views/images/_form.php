<?php
/* @var $this ImagesController */
/* @var $model Images */
/* @var $form TbActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'images-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
    'htmlOptions'=>array('enctype' => 'multipart/form-data')
)); ?>



    <?php echo $form->errorSummary($model); ?>

            <?php //echo$form->textFieldControlGroup($model,'id',array('span'=>5)); ?>

            <?php //echo$form->textFieldControlGroup($model,'photo',array('span'=>5,'maxlength'=>255));
                echo $form->fileFieldControlGroup($model, 'photo');
                if($model->photo) echo CHtml::image(baseUrl().'uploads/'.$model->photo);
            ?>

            <?php echo$form->textFieldControlGroup($model,'description',array('span'=>5,'maxlength'=>255)); ?>

            <?php //echo$form->textFieldControlGroup($model,'rating',array('span'=>5)); ?>

            <?php //echo$form->textFieldControlGroup($model,'created',array('span'=>5)); ?>

            <?php //echo$form->textFieldControlGroup($model,'modified',array('span'=>5)); ?>

        <div class="form-actions">
        <?php echo TbHtml::submitButton($model->isNewRecord ? t('app', 'Create') : t('app','Save'),array(
		    'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
		    'size'=>TbHtml::BUTTON_SIZE_LARGE,
		)); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->