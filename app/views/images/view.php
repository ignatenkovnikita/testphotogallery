<?php
/* @var $this ImagesController */
/* @var $model Images */

$this->pageTitle = 'Последние добавленые фото';

$this->breadcrumbs=array(
    Yii::t('app', 'Images')=>array('/'),
    t('app', 'view')
);

$this->menu=array();
?>

<h1><?php echo Yii::t('app','Photo comment'); ?></h1>

<div class="row">
    <div class="span4"><?php echo  CHtml::image(baseUrl().'uploads/'.$model->photo, '' ,array('style'=>'width:300px;height:200px') );?></div>
    <div class="span4">
        <p><strong>Рейтинг:</strong> <?=$model->ShowRating ?> </p>
        <?
            if($model->description)
                echo "<p><strong>Описание:</strong> $model->description</p>";
        ?>

    </div>
</div>


    <h4 class="anchor" id="comment"><?=t('app','Comments')?></h4>
<? if (!empty($model->comments)) {
    $this->widget('zii.widgets.CListView', array(
        'dataProvider'=> new CActiveDataProvider('Comment',array(
                'data'=>$model->comments,
            )),
        'itemView'=>'//comment/_view',
        'template'=>'{items}',
    ));
} else {?>
    <p><i><?=t('app', 'Comment not found')?></i></p>
<?}?>


    <h4 class="anchor" id="comment"><?=t('app','Add Comments')?></h4>
<?php

$this->renderPartial('/comment/_formComment', array('model'=>$comment));

?>