<?php
/* @var $this ImagesController */
/* @var $model Images */
?>

<?php
$this->breadcrumbs=array(
	Yii::t('app', 'Images')=>array('index'),
	Yii::t('app', 'Update'),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'List Images'), 'url'=>array('index')),
	array('label'=>Yii::t('app', 'Create Images'), 'url'=>array('create')),
);
?>

    <h1><?php Yii::t('app', 'Update Images');?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>