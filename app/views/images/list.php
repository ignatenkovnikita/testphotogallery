<?php
/* @var $this ImagesController */
/* @var $model Images */


$this->pageTitle = 'Последние добавленые фото';

$this->breadcrumbs=array(
    Yii::t('app', 'Images')=>array('/')
);

$this->menu=array();
?>

    <h1><?php echo Yii::t('app','Last Create Images'); ?></h1>

    <div class="thumbnails">
        <?php $this->widget('zii.widgets.CListView', array(
            'dataProvider'=>$model->search(),
            'itemView'=>'_list',
            'sortableAttributes'=>array(
                'rating',
            ),
            //'template'=>'{items} {pager}',
        ));?>
    </div>


