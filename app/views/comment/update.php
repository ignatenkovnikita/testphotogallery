<?php
/* @var $this CommentController */
/* @var $model Comment */
?>

<?php
$this->breadcrumbs=array(
	Yii::t('app', 'Comments')=>array('index'),
	Yii::t('app', 'Update'),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'List Comment'), 'url'=>array('index')),
	array('label'=>Yii::t('app', 'Create Comment'), 'url'=>array('create')),
);
?>

    <h1><?php Yii::t('app', 'Update Comment');?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>