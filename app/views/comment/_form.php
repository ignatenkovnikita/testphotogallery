<?php
/* @var $this CommentController */
/* @var $model Comment */
/* @var $form TbActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'comment-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="help-block"><? echo Yii::t('app', 'Fields with');?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.</p>

    <?php echo $form->errorSummary($model); ?>

            <?php echo$form->textFieldControlGroup($model,'description',array('span'=>5,'maxlength'=>255)); ?>

            <?php echo$form->textFieldControlGroup($model,'rating',array('span'=>5)); ?>

            <?php echo$form->dropDownListControlGroup($model, 'images_id', GxHtml::listDataEx(Images::model()->findAllAttributes(null, true))); ?>

            <?php echo$form->textFieldControlGroup($model,'name',array('span'=>5,'maxlength'=>45)); ?>

            <?php echo$form->textFieldControlGroup($model,'email',array('span'=>5,'maxlength'=>45)); ?>

            <?php echo$form->textFieldControlGroup($model,'user_id',array('span'=>5)); ?>

        <div class="form-actions">
        <?php echo TbHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array(
		    'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
		    'size'=>TbHtml::BUTTON_SIZE_LARGE,
		)); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->