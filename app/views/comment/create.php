<?php
/* @var $this CommentController */
/* @var $model Comment */
?>

<?php
$this->breadcrumbs=array(
	Yii::t('app', 'Comments')=>array('index'),
	Yii::t('app', 'Create'),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'List Comment'), 'url'=>array('index')),
);
?>

<h1><?php echo Yii::t('app','Create Comment'); ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>