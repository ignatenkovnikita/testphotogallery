<?php
/* @var $this CommentController */
/* @var $model Comment */
/* @var $form TbActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'comment-form',
        'action'=>Yii::app()->createUrl('//comment/create'),
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>


    <?php echo $form->errorSummary($model); ?>
            <div class="row">
                <div class="span5">
                    <?php echo$form->textFieldControlGroup($model,'description',array('span'=>5,'maxlength'=>255)); ?>
                </div>

                <div class="span3">
                    <?php
                    echo $form->label($model, 'rating');
                    $this->widget('CStarRating',array('name'=>'Comment[rating]', 'value'=>'5'));
                    ?>
                </div>
            </div>

            <?php echo$form->hiddenField($model, 'images_id'); ?>
            <div class="row">
                <?php

                if(!$model->user_id) {
                    echo "<div class='span4'>";
                    echo$form->textFieldControlGroup($model,'name',array('span'=>4,'maxlength'=>45));
                    echo "</div>";
                    echo "<div class='span4'>";
                    echo$form->textFieldControlGroup($model,'email',array('span'=>4,'maxlength'=>45));
                    echo "</div>";

                }
                ?>

            </div>



            <?php echo$form->hiddenField($model,'user_id'); ?>

        <div class="form-actions">
        <?php echo TbHtml::submitButton($model->isNewRecord ? t('app','Create') : 'Save',array(
		    'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
		    'size'=>TbHtml::BUTTON_SIZE_LARGE,
		)); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->