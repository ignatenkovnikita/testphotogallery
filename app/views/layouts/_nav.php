<?php $this->widget(
	'bootstrap.widgets.TbNavbar', array(
		'color' => TbHtml::NAVBAR_COLOR_INVERSE,
		'brandLabel' => Yii::app()->user->getState('organization'),
		'collapse' => true,
		'items' => array(
			array(
				'class' => 'bootstrap.widgets.TbNav',
				'items' => array(
                    array('label'=> 'Зарегистрироваться','url'=>array('/user/registration'),'visible'=>Yii::app()->user->isGuest),
                    array('label'=> 'Добавить фото', 'url'=> array('/images/create'),'visible'=>!Yii::app()->user->isGuest)
				)
			),
			array(
				'class' => 'bootstrap.widgets.TbNav',
				'items' => array(
                    array('label' => 'Пользователи', 'url' => array('/user'), 'visible' => user()->isAdmin),
                    array('label' => 'Роли', 'url' => array('/auth'), 'visible' => user()->isAdmin),
					array('label' => 'Войти', 'url' => array('/site/login'), 'visible' => user()->isGuest),
					array(
						'label' => 'Выйти ('.user()->name.')',
						'url' => array('/site/logout'),
						'visible' => !user()->isGuest
					)

				)
			)
		)
	)
); ?>


<?php /*$this->widget(
	'bootstrap.widgets.TbNavbar',
	array(
		'collapse' => true,
		'items' => array(
			array(
				'class' => 'bootstrap.widgets.TbNav',
				'items' => array(
					array('label' => 'Объекты', 'url' => array('/object/index')),
					array('label' => 'Категории', 'url' => array('/category/admin'),  'visible' => user()->isAdmin ),
					array('label' => 'Contact', 'url' => array('/site/contact')),
					array('label' => 'Login', 'url' => array('/site/login'), 'visible' => user()->isGuest),
					array(
						'label' => 'Logout (' . user()->name . ')',
						'url' => array('/site/logout'),
						'visible' => !user()->isGuest
					)
				),
			),
		),
	)
); */
?>
