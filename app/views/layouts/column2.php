<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html lang="en">
<?php $this->renderPartial('app.views.layouts._head'); ?>
<body class="layout-main">

<?php $this->renderPartial('app.views.layouts._nav'); ?>

<div class="container" id="page">

    <?php if (!empty($this->breadcrumbs)): ?>
        <?php $this->widget(
            'bootstrap.widgets.TbBreadcrumb',
            array(
                'links' => $this->breadcrumbs,
            )
        ); ?>
    <?php endif ?>

	<div class="container">
		<div class="row">
			<div class="span9">
				<?php echo $content; ?>
			</div>
			<div class="span3">
				<div id="sidebar">

					<?php $this->widget('bootstrap.widgets.TbNav', array(
						'type' => TbHtml::NAV_TYPE_TABS,
						'stacked' => true,
						'items' => $this->menu,

					)); ?>
				</div>
				<!-- sidebar -->
			</div>
		</div>

	</div>



	<hr/>
	<?php $this->renderPartial('app.views.layouts._footer'); ?>

</div>
</body>
</html>
