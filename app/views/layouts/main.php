<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html lang="en">
<?php $this->renderPartial('app.views.layouts._head'); ?>
<body class="layout-main">
<?php $this->renderPartial('app.views.layouts._nav'); ?>

<div class="container" id="page">

    <?php if (!empty($this->breadcrumbs)): ?>
        <?php $this->widget(
            'bootstrap.widgets.TbBreadcrumb',
            array(
                'links' => $this->breadcrumbs,
            )
        ); ?>
    <?php endif ?>

	<div class="container">
		<div class="row">
			<div class="span12">
				<?php echo $content; ?>
			</div>
		</div>

	</div>



    <hr/>
	<?php $this->renderPartial('app.views.layouts._footer'); ?>

</div>
</body>
</html>
