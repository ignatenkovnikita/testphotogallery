<?
/* @var $form TbActiveForm */
/* @var $model User */
echo $form->textFieldControlGroup($model->profile,'telephone');
echo $form->textFieldControlGroup($model->profile,'email');

if ($model->profile->scenario == 'student') {
	echo $form->numberFieldControlGroup($model->profile,'lessonCost');


	echo $form->dropDownListControlGroup($model->profile,'courseId', CHtml::listData(Course::model()->findAll(),'id', 'name'),
		array('empty'=>'-- выберите курс --','ajax' => array(
			'type' => 'POST',
			'url' => CController::createUrl('user/listTeacher'),
			'update' => '#Profile_teacherId'))
	);
	$teacherList = $model->profile->courseId ? User::model()->getUserList(User::ROLE_TEACHER, 'teacherCosts.courseId = '. $model->profile->courseId) : array();
	echo $form->dropDownListControlGroup($model->profile,'teacherId', Chtml::listData($teacherList,'id','name') , array('empty'=>'-- выберите преподавателя --'));
}
else {
    echo $form->dropDownListControlGroup($model->profile, 'languageId', DataList::model()->items('language'));
}