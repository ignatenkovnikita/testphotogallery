<?php
/* @var $this UserController */
/* @var $model User */
?>

<?php
$this->breadcrumbs=array(
	Yii::t('app','Manage Users') => array('admin'),
	$model->title=>array('view','id'=>$model->id),
	Yii::t('app', 'Updating User Data'),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'Delete User'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app','Manage Users'), 'url'=>array('admin')),
	array('label'=>Yii::t('app','Reset password'), 'url'=>array('reset','id'=>$model->id)),
);
?>

<? $this->pageTitle = $model->title; ?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>