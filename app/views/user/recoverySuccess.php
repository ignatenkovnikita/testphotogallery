<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form TbActiveForm */

$this->pageTitle = 'Востановление пароля';
?>
<div>

    <h1>Востановление пароля</h1>

    <div class="alert alert-success" role="alert">Пароль успешно востановлен.<br>Новый пароль отправлен Вам на почту.</div>

</div>