<?php
/* @var $this UserController */
/* @var $model User */
?>

<?php
$this->breadcrumbs=array(
	Yii::t('app','Manage Users') => array('admin'),
	$model->name=>array('view','id'=>$model->id),
	Yii::t('app','Reset password'),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'Delete User'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app','Manage Users'), 'url'=>array('admin')),
	array('label'=>Yii::t('app','Update User'), 'url'=>array('update','id'=>$model->id)),
);
?>
<? $this->pageTitle = Yii::t('app','Reset password'); ?>

<div class="form">

	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
			'id'=>'user-form',
			// Please note: When you enable ajax validation, make sure the corresponding
			// controller action is handling ajax validation correctly.
			// There is a call to performAjaxValidation() commented in generated controller code.
			// See class documentation of CActiveForm for details on this.
			'enableAjaxValidation'=>false,
		)); ?>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->passwordFieldControlGroup($model,'password',array('span'=>5,'maxlength'=>255)); ?>

	<div class="form-actions">
		<?php echo TbHtml::submitButton(Yii::t('app','Reset'),array(
				'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
				'size'=>TbHtml::BUTTON_SIZE_LARGE,
			)); ?>
	</div>

	<?php $this->endWidget(); ?>

</div><!-- form -->