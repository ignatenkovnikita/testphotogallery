<?php
/* @var $this UserController */
/* @var $model User */


$this->breadcrumbs = array(
    Yii::t('app', 'Manage Users'),
);

$this->menu = array(
    array('label' => Yii::t('app', 'Create User'), 'url' => array('create')),
);

?>

<? $this->pageTitle = Yii::t('app', 'Manage Users');

?>
<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'user-grid',
    'dataProvider' => $model,
    //'filter'=>$model,
    'columns' => array(

        array(
            'name' => 'login',
            'value' => 'l($data->title, array("update","id"=>$data->id))',
            'type' => 'raw',
        ),



        /*array(
            'filter'=>DataList::items('roles'),
            'name'=>'profile.roleId',
            'value'=>'$data->profile ? $data->profile->role->name : "-"',
        ),*/
        /*'salt',
        'password',
        'passwordStrategy',
        'requiresNewPassword',
        */
        'lastLoginAt',
        /*'lastActiveAt',
        'status',*/

        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
        ),
    ),
)); ?>