<?php
/* @var $this UserController */
/* @var $model User */
?>

<?php
$this->breadcrumbs = array(
	Yii::t('app', 'Manage Users') => array('admin'),
	Yii::t('app', 'Create User')
);

$this->menu = array(
	array('label' => Yii::t('app', 'Manage Users'), 'url' => array('admin')),
);
?>

	<h1><?= Yii::t('app', 'Create User'); ?></h1>

<?php $this->renderPartial('_form', array('model' => $model,)); ?>