<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form TbActiveForm */

$this->pageTitle = 'Востановление пароля';

$this->breadcrumbs=array(
    Yii::t('app', 'Recovery')
);
?>
<div class="span4">

    <p>Введите свой адрес электронной почты.</p>

    <div class="recovery-form">

        <?php $form = $this->beginWidget(
            'bootstrap.widgets.TbActiveForm',
            array(
                'id' => 'recovery-form',
            )
        ); ?>

        <fieldset>
            <?php echo $form->textFieldControlGroup(
                $model,
                'login',
                array('block' => true, 'label' => false, 'placeholder' => t('app', 'login'))
            ); ?>

        </fieldset>

        <?php echo TbHtml::submitButton(
            t('app','Reset'),
            array('color' => TbHtml::BUTTON_COLOR_PRIMARY, 'size' => TbHtml::BUTTON_SIZE_LARGE, 'block' => true)
        ); ?>

        <?php $this->endWidget(); ?>

    </div>
</div>