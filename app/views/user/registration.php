<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form TbActiveForm */

$this->pageTitle = app()->name . ' - Регистрация';

$this->breadcrumbs=array(
    Yii::t('app', 'Registration')
);
?>
<div class="span4">


    <div class="recovery-form">

        <?php $form = $this->beginWidget(
            'bootstrap.widgets.TbActiveForm',
            array(
                'id' => 'registration-form',
            )
        ); ?>

            <?php echo $form->textFieldControlGroup(
                $model,
                'login',
                array('block' => true, 'label' => false, 'placeholder' => t('app', 'Login'))
            ); ?>

            <?php echo $form->textFieldControlGroup(
                $model,
                'name',
                array('block' => true, 'label' => false, 'placeholder' => t('app', 'Name'))
            ); ?>


            <?php echo $form->passwordFieldControlGroup(
                $model,
                'password',
                array('block' => true, 'label' => false, 'placeholder' => t('app', 'Password'))
            ); ?>




        <?php echo TbHtml::submitButton(
            t('app','Registration'),
            array('color' => TbHtml::BUTTON_COLOR_PRIMARY, 'size' => TbHtml::BUTTON_SIZE_LARGE, 'block' => true)
        ); ?>

        <?php $this->endWidget(); ?>

    </div>
</div>