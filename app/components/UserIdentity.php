<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	private $_model;

	public function authenticate()
	{

		$model = User::model()->findByAttributes(array('login'=>$this->username));
		if ($model) {
			Yii::trace('Try to auth user '.$model->id . ' - '.$model->login);


			if ($model->verifyPassword($this->password)) {
				Yii::trace('Password is valid. Auth complite!');
				$this->errorCode=self::ERROR_NONE;

				Yii::trace('Set user params to identity');
				$this->_model = $model;

				//$this->setState('roleId',$this->_model->profile->roleId);

			} else {
				Yii::trace('Password is NOT valid.');
				$this->errorCode=self::ERROR_PASSWORD_INVALID;
			}
		} else {
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		}


		return !$this->errorCode;
	}

	public function getId() {
		return $this->_model->id;
	}


	public function getName() {
		return $this->_model->login;
	}

	public function getRoleId() {
		return $this->_model->profile->roleId;
	}



}