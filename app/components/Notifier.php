<?php

class Notifier extends CComponent {

	public function init() {

	}

	public static function newUser($event){
		$user = $event->sender;

		self::initMail();

		app()->mailer->AddAddress($user->login);
		app()->mailer->Subject = "Для вас создана учетная запись";
		app()->mailer->getView('newUser', array('user'=>$user), null);

		//app()->mailer->Send();
	}

    public static function resetPass($event){
            $user = $event->sender;

            self::initMail();

            app()->mailer->AddAddress($user->login);
            app()->mailer->Subject = "Для вас сброшен пароль";
            app()->mailer->getView('resetPass', array('user'=>$user, 'newPass'=>$event->params['newPass']), null);
            //app()->mailer->Send();
    }

    public static function initMail() {
		app()->mailer->ClearAddresses();
		app()->mailer->ClearCCs();
		app()->mailer->ClearBCCs();
		app()->mailer->ClearReplyTos();
		app()->mailer->ClearAllRecipients();
		app()->mailer->ClearAttachments();
		app()->mailer->ClearCustomHeaders();

		app()->mailer->From = app()->params['adminEmail'];
		app()->mailer->FromName = "";
		app()->mailer->WordWrap = 100;                                 // set word wrap to 50 characters
		app()->mailer->IsHTML(true);
		app()->mailer->CharSet = 'UTF-8';
	}

}