<?php
/**
 * Created by PhpStorm.
 * User: vasya
 * Date: 08.12.14
 * Time: 2:47
 */

class CategoryWidget extends CWidget {

    public function run() {

        $criteria = new CDbCriteria();
        $criteria->select = 'title, count(name) as count, t.alias';
        $criteria->addCondition('active_type_id='.Organization::ACTIVE_PUBLISHED) ;
        $criteria->group = 't.title';
        $criteria->order = 'title ';
        //$criteria->join = 'INNER JOIN organization';
        $criteria->with = 'organizations';

        $model = Category::model()->findAll($criteria);


        $this->render('application.views.widget.CategoryWidget', compact('model'));
    }


}