<?php

Yii::import('application.models.ar._base.BaseComment');

class Comment extends BaseComment
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class'=>'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate'=>false,
                'createAttribute'=>'created',
                'updateAttribute'=>'modified',
            ),
        );
    }

    protected function afterSave() {
        parent::afterSave();

        $images = $this->images;
            //$org->rating = round($org->rating + ($this->rating * 100 - $org->rating) / ($org->CommentCount + 1));

        $a = $this->rating * 100 - $images->rating;
        $b = $images->CommentCount;

        $images->rating = round($images->rating + $a/$b);
            //exit();
        $images->save();
    }

    public function getFioName() {
        if($this->user_id) {
            $user = User::model()->findByPk($this->user_id);
            return "$user->name ($user->login)";
        }
        else {
            return "$this->name ($this->email)";
        }
    }

    public function getFormattedRating()
    {
        $rating = $this->rating / 100;
        $rating = number_format(round($rating * 4) / 4, 2);
        $rating = sprintf("%1.2f", $rating);
        $rating = trim(rtrim($rating, "0"), '.');
        return $this->rating;
    }

    public function getShowRating()
    {
        $good_rating = $this->getFormattedRating();
        echo '<span class="pull-right">'.(CHtml::encode(dateFormatter()->formatDateTime($this->created, 'long', 'short') )).'</span> <div class="pull-right x-rating x-rate-' .$good_rating. '"></div>';
    }



}