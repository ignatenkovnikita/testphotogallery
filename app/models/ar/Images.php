<?php

Yii::import('application.models.ar._base.BaseImages');

class Images extends BaseImages
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    private $oldPhoto;

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class'=>'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate'=>false,
                'createAttribute'=>'created',
                'updateAttribute'=>'modified',
            ),
        );
    }

    protected function beforeSave()
    {
        // add photo
        $this->photo = CUploadedFile::getInstance($this, 'photo');
        if($this->photo) {
            $tempName = uniqid().'.'.$this->photo->getExtensionName();
            $dir = Yii::getPathOfAlias('webroot.uploads').DIRECTORY_SEPARATOR;
            $this->photo->saveAs($dir.$tempName);
            $this->photo = $tempName;
            // resize
            //$params = array('w'=>200);
            //Yii::app()->resizeManager->resize($dir.$tempName, $dir.$tempName, $params);
        }
        else
            $this->photo = $this->oldPhoto;

        $this->description = purify($this->description);

        return parent::beforeSave();
    }

    public function  afterFind()
    {
        parent::afterFind();
        $this->oldPhoto = $this->photo;
    }

    public function getCommentCount() {
        return Comment::model()->count("images_id=:images_id", array(":images_id" => $this->id));
    }

    public function getFormattedRating()
    {
        $rating = $this->rating / 100;
        $rating = number_format(round($rating * 4) / 4, 2);
        $rating = sprintf("%1.2f", $rating);
        $rating = trim(rtrim($rating, "0"), '.');
        return round($rating);
    }

    public function getShowRating()
    {
        $good_rating = $this->getFormattedRating();
        echo '<div class="x-rating x-rate-' .$good_rating. '"></div>';
    }
}