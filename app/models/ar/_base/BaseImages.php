<?php

/**
 * This is the model base class for the table "images".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Images".
 *
 * Columns in table "images" available as properties of the model,
 * followed by relations of table "images" available as properties of the model.
 *
 * @property integer $id
 * @property string $photo
 * @property string $description
 * @property integer $rating
 * @property string $created
 * @property string $modified
 *
 * @property Comment[] $comments
 */
abstract class BaseImages extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'images';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'Images|Images', $n);
	}

	public static function representingColumn() {
		return 'photo';
	}

	public function rules() {
		return array(
			array('rating', 'numerical', 'integerOnly'=>true),
			array('photo, description', 'length', 'max'=>255),
			array('created, modified', 'safe'),
			array('photo, description, rating, created, modified', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, photo, description, rating, created, modified', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'comments' => array(self::HAS_MANY, 'Comment', 'images_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'photo' => Yii::t('app', 'Photo'),
			'description' => Yii::t('app', 'Description'),
			'rating' => Yii::t('app', 'Rating'),
			'created' => Yii::t('app', 'Created'),
			'modified' => Yii::t('app', 'Modified'),
			'comments' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('photo', $this->photo, true);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('rating', $this->rating);
		$criteria->compare('created', $this->created, true);
		$criteria->compare('modified', $this->modified, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}