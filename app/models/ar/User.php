<?php

Yii::import('application.models.ar._base.BaseUser');
Yii::import('vendor.phpnode.yiipassword.*');

class User extends BaseUser
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return array the behavior configurations (behavior name=>behavior configuration)
	 */
	public function behaviors()
	{
		return array_merge(parent::behaviors(), array(
				'password' => array(
					'class' => 'APasswordBehavior',
					'defaultStrategyName' => 'legacy',
					'strategies' => array(
						'bcrypt' => array(
							'class' => 'ABcryptPasswordStrategy',
							'workFactor' => 12,
						),
						"legacy" => array(
							"class" => "ALegacyMd5PasswordStrategy",
						),
					),
				),
				'workflow' => array(
					'class' => 'vendor.crisu83.yii-workflow.behaviors.WorkflowBehavior',
					'defaultStatus' => self::STATUS_DEFAULT,
					'statuses' => array(
						self::STATUS_DEFAULT => array(
							'label' => t('label', 'Default'),
							'transitions' => array(self::STATUS_DELETED),
						),
						self::STATUS_DELETED => array(
							'label' => t('label', 'Deleted'),
						),
					),
				),
			));
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array_merge(parent::rules(), array(
				array('login', 'unique'),
				array('requiresNewPassword', 'numerical', 'integerOnly' => true),
				array('name, salt, password, passwordStrategy', 'length', 'max' => 255),
				// The following rule is used by search().
				array('id, name, lastLoginAt, lastActiveAt', 'safe', 'on' => 'search'),

                // For registration
                array('login, password', 'required',  'on'=>'reg'),
                array('login',    'match',   'pattern'    => '/^([a-z0-9_\.-]+)@([a-z0-9_\.-]+)\.([a-z\.]{2,6})$/', 'message' => 'Не верный формат e-mail адреса.', 'on'=>'reg'),
			));
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array_merge(parent::relations(), array(
			));
	}

	public function listUsers($condition) {
		$criteria = new CDbCriteria();
		$criteria->with = array('profile');
		$criteria->condition = $condition;

		$criteria->order = 'name';
		return CHtml::listData($this->findAll($criteria),'id','name');
	}


	public function getTitle() {
		return '#'.$this->id.' '. $this->name . ' ('.$this->login.')';
	}

	public function getList($criteria) {
		$users = $this->findAll(array('with'=>'profile','condition'=>$criteria));
		return CHtml::listData($users, 'id', 'name');
	}

	/**
	 * @param      $roleId
	 * @param null $addCriteria
	 *
	 * @return static[]
	 * используется для формирования список с учетом профиля, и курсов (для препода)
	 */
	public function getUserList($roleId, $addCriteria  = null) {
		$criteria = new CDbCriteria();
		$criteria->with = array('teacherCosts', 'profile');
		$criteria->addCondition('profile.roleId = '. $roleId);
		if ($addCriteria) {
			$criteria->addCondition($addCriteria);
		}

		//$criteria->addCondition('teacherCosts.courseId = '. $courseId);

		return $this->findAll($criteria);
	}

    public function random_password( $length = 8 ) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
        $password = substr( str_shuffle( $chars ), 0, $length );
        return $password;
    }

}