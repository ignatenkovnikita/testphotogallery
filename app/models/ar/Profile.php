<?php

Yii::import('application.models.ar._base.BaseProfile');

class Profile extends BaseProfile
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function getFullInfo()
    {
        return $this->user->name;
    }

    public function rules()
    {
        return array_merge(
            parent::rules(),

            array(
                // поля для всех
                array('email', 'email'),
                array('email', 'unique'),
                // поля для студентов
                array('lessonCost, teacherId, courseId', 'required', 'on' => 'student'),
                array('lessonCost', 'numerical', 'on' => 'student'),
                array('teacherId', 'numerical', 'on' => 'student'),
                array('courseId', 'numerical', 'on' => 'student'),
            )
        );
    }

    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['teacherId'] = t('app', 'Teacher');
        $labels['languageId'] = t('app', 'Language');

        $labels['roleId'] = t('app', 'Role');
        $labels['courseId'] = t('app', 'Cource');
        $labels['leftLessonCount'] = t('app', 'Left Lesson Count');

        return $labels;
    }

    public function detailData(User $model)
    {
        $all = array('profile.email', 'profile.telephone');
        $add = array();
        if ($model->profile->roleId == User::ROLE_STUDENT) {
            $add = array('profile.lessonCost',
                array(
                    'name' => 'profile.teacherId',
                    'value' => $model->profile->teacher->name,
                )
            );
        }
        else $add = array(
            array(
                'name'=>'languageId',
                'value'=>$model->profile->language->name,
            ),
        );

        return array_merge($all, $add);
    }


    /**
     * @return float - Возвращает кол-во оставшихся уроков
     */
    public function getLeftLessonCount()
    {
        return floor($this->bill / $this->lessonCost);
    }


}