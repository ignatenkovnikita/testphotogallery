<?php

/**
 * RecoveryForm class.
 * RecoveryForm is the data structure for keeping
 * user login form data. It is used by the 'forgot' action of 'SiteController'.
 */
class RecoveryForm extends CFormModel
{
    public $login;


    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'login' => t('app', 'Login'),
        );
    }
    public function rules() {
        return array(
            array('login', 'required'),
            array('login', 'email','message'=>t('app',"The email isn't correct")),
            array('login', 'isEmailExists'),
        );
    }

    public function isEmailExists($attribute, $params)
    {
        //$profile = Profile::model()->exists('email = :email',array(':email'=>$this->email));
        $user = User::model()->findByAttributes(array('login'=>$this->login));
        if(!$user)
            $this->addError($attribute, t('app', 'Email not exists!'));
    }


}
