<?php
// web application configuration
return array(
	//'theme'=>'serenity',
    // path aliases
    'aliases' => array(
        'bootstrap' => 'vendor.crisu83.yiistrap',
		'xupload' => 'vendor.asgaroth.xupload',
    ),
    // application behaviors
    'behaviors' => array(
        'maintain' => array(
            'class' => 'vendor.crisu83.yii-consoletools.behaviors.MaintainApplicationBehavior',
            'maintainFile' => __DIR__ . '/../runtime/maintain',
        ),
    ),

	'defaultController'=>'images/list',

    // controllers mappings
    'controllerMap' => array(
    ),
    // application modules
    'modules' => array(
		'auth' => array(
			'strictMode' => true, // when enabled authorization items cannot be assigned children of the same type.
			'userClass' => 'User', // the name of the user model class.
			'userIdColumn' => 'id', // the name of the user id column.
			'userNameColumn' => 'title', // the name of the user name column.
			'defaultLayout' => 'application.views.layouts.column2', // the layout used by the module.
			'viewDir' => null, // the path to view files to use with this module.
		),
	   /* 'userModule' => array(
		    'class' => 'vendor.petun.yii-usermanager.UserModule',
		    //'profileModel' => 'Profile',
		    //'useProfile' => true,
		    'profileForm' => 'application.views.profile._userForm',
		    'profileView' => false, // устарело
		    'eventOnUserCreate' => array('Notifier', 'newUser'), // для уведомлений о новой учетной записи.
	    ),*/
		'dataList' => array(
			'class' => 'vendor.petun.yii-listmanager.DataListModule',
			'lists' => array(
				'roles'=> 'Роли',
                'active_type' => 'Статусы активности',
                'category_type' => 'Категория организации',
                'rating_type' => 'Статусы рейтингов',
                'contact_type' => 'Тип контакта',
                'address_type' => 'Тип адреса',

			)
		)
    ),
    // application components
    'components' => array(

        'notifier'=> array(
            'class'=>'app.components.Notifier',
        ),
        'mailer' => array(
            'class' => 'application.extensions.mailer.EMailer',
            'pathViews' => 'application.views.email',
            'pathLayouts' => 'application.views.email.layouts'
        ),
        'bootstrap' => array(
            'class' => 'bootstrap.components.TbApi',
        ),
        'log' => array(
            /*'routes' => array(
                array(
                    'class' => 'vendor.malyshev.yii-debug-toolbar.YiiDebugToolbarRoute',
                    'ipFilters' => array('127.0.0.1', '10.0.2.2', '::1'),
                ),
            ),*/
        ),
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            //'urlSuffix' => '.html',
            'rules' => array(

                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ),
        'user' => array(
            'class'=>'auth.components.AuthWebUser',
            'allowAutoLogin' => true,
	        'admins' => array('admin'),
        ),
        'errorHandler' => array(
            'errorAction' => 'site/error',
        ),
		'authManager' => array(
			'behaviors' => array(
				'auth' => array(
					'class' => 'auth.components.AuthBehavior',
				),
			),
			'class'=>'CDbAuthManager',
			'connectionID'=>'db',
			'defaultRoles'=>array('user', 'guest'),
		),
		'resizeManager' => array(
			'class' => 'vendor.petun.yii-image-resizer.PImageResizer'
		),
    ),
);
