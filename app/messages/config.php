<?php

return array(
    'sourcePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'messagePath' => dirname(__FILE__),
    'languages' => array('ru'),
    'fileTypes' => array('php'),
    'overwrite' => true,
    'translator' => array('t','Yii::t'),
    'exclude' => array(
        '.svn',
        '/generators',
        '/messages',
        '/vendors',
    ),
);